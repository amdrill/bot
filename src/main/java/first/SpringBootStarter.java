package first;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootStarter {


	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootStarter.class, args);
	}
}