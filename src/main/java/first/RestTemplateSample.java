package first;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateSample {

	public static void submitMessage() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Host", "discordapp.com");
		headers.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0");
		headers.set("Accept", "*/*");
		headers.set("Accept-Language", "en-US");
		headers.set("Accept-Encoding", "gzip, deflate, br");
		headers.set("Referer", "https://discordapp.com/channels/252776251708801024/380275290741538817");
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", "NTA0ODkxMTU4Mjg4MjY5MzQ0.DwAmtg.UASdm5BfRuT-8BxwVZjHOT80fIs");
		headers.set("X-Super-Properties",
				"eyJvcyI6IldpbmRvd3MiLCJicm93c2VyIjoiRmlyZWZveCIsImRldmljZSI6IiIsImJyb3dzZXJfdXNlcl9hZ2VudCI6Ik1vemlsbGEvNS4wIChXaW5kb3dzIE5UIDEwLjA7IFdpbjY0OyB4NjQ7IHJ2OjY0LjApIEdlY2tvLzIwMTAwMTAxIEZpcmVmb3gvNjQuMCIsImJyb3dzZXJfdmVyc2lvbiI6IjY0LjAiLCJvc192ZXJzaW9uIjoiMTAiLCJyZWZlcnJlciI6Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vIiwicmVmZXJyaW5nX2RvbWFpbiI6Ind3dy5nb29nbGUuY29tIiwic2VhcmNoX2VuZ2luZSI6Imdvb2dsZSIsInJlZmVycmVyX2N1cnJlbnQiOiIiLCJyZWZlcnJpbmdfZG9tYWluX2N1cnJlbnQiOiIiLCJyZWxlYXNlX2NoYW5uZWwiOiJzdGFibGUiLCJjbGllbnRfYnVpbGRfbnVtYmVyIjozMDI4MywiY2xpZW50X2V2ZW50X3NvdXJjZSI6bnVsbH0=");
		headers.set("Content-Length", "64");
		headers.set("Connection", "keep-alive");
		headers.set("Cookie",
				"__cfduid=d9c33af21ae2e4b6fc3a3e2d536e152861545507169; locale=en-US; _ga=GA1.2.511625238.1545507172; _gid=GA1.2.320648751.1545507172");
		headers.set("TE", "Trailers");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("content", "!tg train");
		map.add("nonce", System.currentTimeMillis() + "");
		map.add("tts", "false");

		String value = "{\"content\":\"!tg train\",\"nonce\":\"" + System.currentTimeMillis() + "\",\"tts\":false}";

		String fooResourceUrl = "https://discordapp.com/api/v6/channels/380275290741538817/messages";

		HttpEntity<String> request = new HttpEntity<>(value, headers);

		ResponseEntity<String> response = restTemplate.postForEntity(fooResourceUrl, request, String.class);

//		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
//		System.out.println(response);
	}

	public static void main(String[] args) {
		RestTemplateSample sample = new RestTemplateSample();
		sample.submitMessage();
	}
}
